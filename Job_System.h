#pragma once

#include <queue>
#include <stdio.h>
#include <stdint.h>
#include <Windows.h>
#include <iostream>
#include <assert.h>
#include "Job.h"


class job_system
{
public:
	job_system(uint32_t worker_thread_count);

	~job_system(void);

	void Add_Job(job* j);

	void Quit_Job();

	uint64_t Get_Thread_Count()
	{
		return Thread_count;
	}

private:
	void do_worker_thread(void);
	static DWORD worker_thread(void* user_data);

	SRWLOCK mutex;
	HANDLE semaphore;
	HANDLE* threads;
	std::queue<job*> job_queue;
	bool Thread_Shutdown;
	uint64_t Thread_count;


};

