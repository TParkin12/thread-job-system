#pragma once
#include "Job.h"
#include <iostream>
#include "Job_System.h"



class job_a : public job
{
public:
	job_a(int Num_elements, int* n)
		: Number_of_Elements(Num_elements), numbers(n), Sum{0}
	{}

	void do_job(void) override	
	{
		for (int i = 0; i < Number_of_Elements; ++i)
		{
			Sum += numbers[i]; // do the sumation of chunk 
		}
	}


	uint64_t Get_Sum()
	{
		return Sum;
	}
	

private:
	int* numbers;
	uint64_t Number_of_Elements;
	uint64_t Sum;
};

class job_b : public job
{
public:
	job_b(int Num_elements)
		: Number_of_Elements(Num_elements)
	{}

	void do_job(void) override
	{
		srand(5);
		numbers = (int*)malloc(sizeof(int) * Number_of_Elements);
		for (int i = 0; i < Number_of_Elements; ++i)
		{
			numbers[i] = rand();
		}
	}


	int* Get_Numbers()
	{
		return numbers;
	}


private:
	int* numbers;
	uint64_t Number_of_Elements;

};

