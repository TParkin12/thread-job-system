/////////////////////////////////////////////////////////////////////////////////
//Creating threads and joining them to calculate average of a sum from an array//
/////////////////////////////////////////////////////////////////////////////////
//#include <stdio.h>
//#include <stdint.h>
//#include <vector>
//#include <Windows.h>
//#include "PerformanceTimer.h"
//#include <time.h>
//
//static int64_t count;
//int* numbers;
//uint64_t Average;
//static SRWLOCK mutex;
//Timer timer;
//const uint64_t thread_count = 16;
//int sum[thread_count] = { 0 };
//const int num_count = 16;
//int totalsum = 0;
//
//void RandomNumbers()
//{
//    srand(5);
//
//    
//    numbers = (int*)malloc(sizeof(int) * num_count);
//    for (int i = 0; i < num_count; ++i)
//        numbers[i] = rand();
//}
//
//DWORD thread_entry(void* user_data)
//{
//    const uint64_t thread_index = (uint64_t)user_data;
//
//       //AcquireSRWLockExclusive(&mutex);
//       //ReleaseSRWLockExclusive(&mutex);
//
//        //alternate way
//        //_InterlockedIncrement64(&count);
//
//       for (int i = thread_index * (num_count / thread_count); i < (thread_index + 1) * (num_count / thread_count); ++i ) //split array into chunks of amount = to thread count
//       {
//           sum[thread_index] += numbers[i]; // do the sumation of each chunk 1 by 1
//       }
//
//    return 0;
//}
//
//int main(void)
//{
//    InitializeSRWLock(&mutex); 
//
//   
//    HANDLE thread_handles[thread_count];
//
//     
//    RandomNumbers();
//    timer.start();
//    for (uint64_t i = 0; i < thread_count; ++i)
//        thread_handles[i] = CreateThread(nullptr, 0, thread_entry, (void*)i, 0, nullptr);
//    for (uint64_t i = 0; i < thread_count; ++i)
//    {
//        //join each chunk
//        WaitForSingleObject(thread_handles[i], INFINITE);
//        CloseHandle(thread_handles[i]);
//        
//    }
//
//    for (int j = 0; j < thread_count; ++j)
//    {
//        totalsum += sum[j]; //total each chunks sum
//    }
//    Average = totalsum / num_count; // average of the total
//
//   
//    
//    timer.stop();
//   
//
//    printf("Average of %d calculated in %f seconds", Average, timer.elapsed());
//}
//
//






/////////////////////////////////////////////////////////////////////////////////
//Job system for threads                                                       //
/////////////////////////////////////////////////////////////////////////////////

//struct job;
//using job_function = void(*)(job* j);
//
//struct job
//{
//	job_function function;
//	void* data;
//};

#include "PerformanceTimer.h"
#include "Job_System.h"
#include "Create_Job.h"
#include <assert.h>

int main(void)
{
	

	
	
		Timer timer;
		int* numbers = 0;
		const int ArraySize = (1024 * 1024);
		const int Number_Of_Jobs = 16;
		const int Job_Chunk = ArraySize / Number_Of_Jobs;
		job* j = nullptr;
		uint64_t totalsum = 0;
		uint64_t Average;
		uint64_t sum[Number_Of_Jobs] = { 0 };
		std::vector<job_a*> Jobs;
		std::vector<job_b*> RandJob;
		int count = 0;

		timer.start();


		job_system* system = new job_system(Number_Of_Jobs); // 16 = thread count

//-----------------------------------------------------------------------------------------------------------
		///
		/// Testing Code for Creating Random numbers using threads
		/// 
		for (int i = 0; i < system->Get_Thread_Count(); ++i)
		{

			job_b* j = new job_b(Job_Chunk);
			system->Add_Job(j);
			RandJob.push_back(j);

		}

		
		for (auto& s : RandJob)
		{
			numbers = s->Get_Numbers();
		}
//-----------------------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------------------
		///
		/// Uses threads to sum a chunk of numbers, then joins them together into one sum
		/// 
		for (int i = 0; i < system->Get_Thread_Count(); ++i)
		{

			job_a* j = new job_a(Job_Chunk, &numbers[i * (ArraySize / system->Get_Thread_Count())]);
			system->Add_Job(j);
			Jobs.push_back(j);

		}
		delete system;
		delete j;

		
		for (auto& s : Jobs)
		{

			sum[count] = s->Get_Sum();
			count++;
		}


		for (int i = 0; i < Number_Of_Jobs; ++i)
		{
			totalsum += sum[i]; //total each chunks sum
		}
		Average = (totalsum / ArraySize);
//-----------------------------------------------------------------------------------------------------------
		timer.stop();

		printf("Average of %d calculated in %f seconds", Average, timer.elapsed());
	}

	
