#pragma once
#include <iostream>
#include <time.h>
#include <chrono>
#include <windows.h>

using namespace std::chrono;
class Timer
{
public:
    Timer(void);

    void start(void);
    void stop(void);

    double elapsed(void) const;

private:
    LARGE_INTEGER frequency;
    LARGE_INTEGER begin;
    LARGE_INTEGER end;

};

Timer::Timer(void)
    : begin(), end()
{
    QueryPerformanceFrequency(&frequency);

}

void Timer::start(void)
{
    QueryPerformanceCounter(&begin);
}
void Timer::stop(void)
{
    QueryPerformanceCounter(&end);
}
double Timer::elapsed(void) const
{
    LARGE_INTEGER ElapsedMicroseconds;
    ElapsedMicroseconds.QuadPart = end.QuadPart - begin.QuadPart;

    ElapsedMicroseconds.QuadPart *= 1000000000;
    ElapsedMicroseconds.QuadPart /= frequency.QuadPart;

    double seconds = (double)ElapsedMicroseconds.QuadPart / 1000000000;

    return seconds;
}