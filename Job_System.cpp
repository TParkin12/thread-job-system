#include "Job_System.h"

job_system::job_system(uint32_t worker_thread_count)
	:Thread_count(worker_thread_count), Thread_Shutdown(false)
{
	


	semaphore = CreateSemaphoreA(nullptr, 0, LONG_MAX, "Job Queue");
	InitializeSRWLock(&mutex);
	threads = (HANDLE*)malloc(sizeof(HANDLE) * Thread_count);

	for (uint64_t i = 0; i < Thread_count; ++i)
		threads[i] = CreateThread(nullptr, 0, worker_thread, this, 0, nullptr);
	



}

void job_system::do_worker_thread(void)
{
	for (;;)
	{
		//wait for job posting ( wait is same as decrement)
		WaitForSingleObject(semaphore, INFINITE);
		//get Job
		job* j = nullptr;
		AcquireSRWLockExclusive(&mutex); 
		if (!job_queue.empty())
		{
			j = job_queue.front();
			job_queue.pop();
		}
		ReleaseSRWLockExclusive(&mutex);

		//do job
		if (j)
			j->do_job();
		else if (Thread_Shutdown)
			break;
		else
			assert(false);
	}
}

DWORD job_system::worker_thread(void* user_data)
{

	job_system* system = (job_system*)user_data;
	system->do_worker_thread();
	return 0;
}

void job_system::Quit_Job()
{
	Thread_Shutdown = true;
}

void  job_system::Add_Job(job* j)
{
	AcquireSRWLockExclusive(&mutex);
	job_queue.push(j);
	ReleaseSRWLockExclusive(&mutex);
	ReleaseSemaphore(semaphore, 1, nullptr);


	//Quit_Job();

	//ReleaseSemaphore(semaphore, Thread_count, nullptr);

	// asynch_update_physics();
	// update.ai();
	// wait_for_physics();
	// update_anim();
}



job_system::~job_system(void)
{
	Quit_Job();
	ReleaseSemaphore(semaphore, Thread_count, nullptr);

	for (uint64_t i = 0; i < Thread_count; ++i)
	{
		// Join
		WaitForSingleObject(threads[i], INFINITE);
		CloseHandle(threads[i]);
	}

	CloseHandle(semaphore);
	free(threads);
}

